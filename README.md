# Programming Test
Write a small Android chat application which satisfies the following minimum requirements:

1) Has an home page where user can see and select 3 conversations
2) Clicking a conversation opens the details page where user can read the related messages
3) User can write a new message, and she will receive an answer (any answer) to it after a short delay, for instance 2s

Conversations and relative messages can be retrieved from:

https://idtm-media.s3.amazonaws.com/programming-test/api/inbox.json

https://idtm-media.s3.amazonaws.com/programming-test/api/9991.json

https://idtm-media.s3.amazonaws.com/programming-test/api/9992.json

https://idtm-media.s3.amazonaws.com/programming-test/api/9993.json

Create a git repository for the programming test. Commits should be atomic. Repository should be sent as zip archive, or shared via the Github or the Gitlab.

==================================================
# My stuff

Companies asking for take-home assignment should send a starting template.
The starting template came from https://github.com/blocoio/android-template .
Starting and structuring a project from scratch takes much time, candidates should have their life simplified and focus only on the project implementation.

## 🏘️ Architecture
I use **MVVM**, which is de facto Clean Architecture promoted by Google.
I experiment Hilt (still in alpha) as Dependency Injection. Hilt is built on top of Dagger2.

## 🕵️‍ How to run

Clone this project
>git clone https://gitlab.com/raymond.chenon/messages.git

Open in [Android Studio](https://developer.android.com/studio) the file `build.gradle` (the one at root folder).

In the terminal enter this command:
`./gradlew build`

## 🦁 TODO
 - add persistence with Room to save the message history. I've done it in https://github.com/raychenon/gray-sky-weather.git
 - different view type in `DetailAdapter` for "me" and "others"
 - There are already many mock and Unit testing. More Unit tests won't hurt.
 - UI testing
 - nice animations (ex: Lottie)

## 🎁 Licence
This app is released under the [Apache License 2.0](https://github.com/raychenon/gray-sky-weather/blob/master/LICENSE).

You can read [the terms of Apache License 2.0 in simple English](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0))
