package io.betterapps.message.domain.utils

import io.betterapps.message.utils.TimeUtils
import org.junit.Assert
import org.junit.Test

class TimeUtilsTest {

    @Test
    fun hourTest() {
        val time = 1599814026153L
        Assert.assertEquals("08:47", TimeUtils.formatLocalTime(time))
    }

    @Test
    fun hour2Test() {
        val time = 1599000026153L
        Assert.assertEquals("22:40", TimeUtils.formatLocalTime(time))
    }

    @Test
    fun hour3Test() {
        val time = 1512814026153L
        Assert.assertEquals("10:07", TimeUtils.formatLocalTime(time))
    }

}