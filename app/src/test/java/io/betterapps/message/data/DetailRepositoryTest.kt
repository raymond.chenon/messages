package io.betterapps.message.data

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import io.betterapps.message.data.network.DetailService
import io.betterapps.message.domain.dto.Message
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class DetailRepositoryTest {


    @Test
    fun addMessageGroupTest() {
        val service = mock<DetailService>()
        val repository = DetailRepository(service)

        // before
        Assert.assertEquals(0, repository.cache.size)

        val key = "1005"
        val response =
            listOf<Message>(Message("1003", "How about tomorrow then?", 1599814026153, "John"))
        service.stub {
            onBlocking { getMessages(key) }.doReturn(response)
        }

        val receivedValue = runBlocking<List<Message>> {
            repository.getMessages(key)
        }

        Assert.assertEquals(1, repository.cache.size)
        Assert.assertEquals(response, repository.cache[key])

        Assert.assertEquals(response, receivedValue)
    }
}