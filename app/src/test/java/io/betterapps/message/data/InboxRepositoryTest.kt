package io.betterapps.message.data

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import io.betterapps.message.data.network.InboxService
import io.betterapps.message.domain.dto.Inbox
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class InboxRepositoryTest {

    @Test
    fun loadFromCache() {
        val service = mock<InboxService>()


        val response =
            listOf<Inbox>(
                Inbox(
                    "9991",
                    "How about tomorrow then?",
                    listOf("John", "Daniel", "Rachel"),
                    "pizza night",
                    1599814026153L
                ),
                Inbox(
                    "9992",
                    "I will send them to you asap",
                    listOf("Raphael"),
                    "slides",
                    1599000026153
                ),
                Inbox(
                    "9993",
                    "Can you please?",
                    listOf("Mum", "Dad", "Bro"),
                    "pictures",
                    1599814026153
                )
            )

        service.stub {
            onBlocking { getHomeMessages() }.doReturn(response)
        }

        val repo = InboxRepository(service)
        // cache is empty
        Assert.assertEquals(null, repo.cache)

        val value = runBlocking<List<Inbox>> {
            repo.getHomeMessages()
        }

        Assert.assertEquals(value, response)

        // load a second time
        Assert.assertEquals(response, repo.cache)
        Assert.assertEquals(value, response)
    }
}