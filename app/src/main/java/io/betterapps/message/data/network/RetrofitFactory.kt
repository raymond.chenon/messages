package io.betterapps.message.data.network

import io.betterapps.message.const.GlobalConstants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitFactory {

    companion object {

        fun createRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(GlobalConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //  .client(client) //client: OkHttpClient
                .build()
        }

        fun homeService(): InboxService {
            return createRetrofit().create(InboxService::class.java)
        }

        fun detailService(): DetailService {
            return createRetrofit().create(DetailService::class.java)
        }
    }

}