package io.betterapps.messages.data.coroutines

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
