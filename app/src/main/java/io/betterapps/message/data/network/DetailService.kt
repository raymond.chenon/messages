package io.betterapps.message.data.network

import io.betterapps.message.domain.dto.Message
import retrofit2.http.GET
import retrofit2.http.Path

//https://idtm-media.s3.amazonaws.com/programming-test/api/9991.json
interface DetailService {

    @GET("{id}.json")
    suspend fun getMessages(@Path("id") id: String): List<Message>
}