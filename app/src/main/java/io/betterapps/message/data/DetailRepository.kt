package io.betterapps.message.data

import androidx.annotation.VisibleForTesting
import io.betterapps.message.data.network.DetailService
import io.betterapps.message.domain.dto.Message
import javax.inject.Inject

class DetailRepository @Inject constructor(
    private val service: DetailService
) {
    @VisibleForTesting
    var cache: MutableMap<String, List<Message>> // should be private

    init {
        cache = mutableMapOf()
    }

    suspend fun getMessages(id: String): List<Message> {
        return cache[id] ?: fetchAndCache(id)
    }

    private suspend fun fetchAndCache(id: String): List<Message> {
        val response = service.getMessages(id)
        cache[id] = response
        return response
    }
}