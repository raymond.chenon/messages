package io.betterapps.message.data.network

import io.betterapps.message.domain.dto.Inbox
import retrofit2.http.GET


interface InboxService {

    @GET("inbox.json")
    suspend fun getHomeMessages(): List<Inbox>

}