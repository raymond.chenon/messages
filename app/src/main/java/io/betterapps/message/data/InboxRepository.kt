package io.betterapps.message.data

import androidx.annotation.VisibleForTesting
import io.betterapps.message.data.network.InboxService
import io.betterapps.message.domain.dto.Inbox
import javax.inject.Inject

class InboxRepository @Inject constructor(
    private val service: InboxService
) {
    @VisibleForTesting
    var cache: List<Inbox>? = null // should be private

    suspend fun getHomeMessages(): List<Inbox> {
        return cache ?: fetchAndCache()
    }

    private suspend fun fetchAndCache(): List<Inbox> {
        val response = service.getHomeMessages()
        cache = response
        return response
    }
}