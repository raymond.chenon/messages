package io.betterapps.message.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import io.betterapps.message.data.InboxRepository
import io.betterapps.message.domain.dto.Inbox
import io.betterapps.message.ui.BaseViewModel
import io.betterapps.messages.data.coroutines.Resource
import kotlinx.coroutines.Dispatchers

class HomeViewModel
@ViewModelInject constructor(
    val repository: InboxRepository
) : BaseViewModel() {

    fun requestHomeMessages(
    ): LiveData<Resource<List<Inbox>>> =
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(repository.getHomeMessages()))
            } catch (exception: Exception) {
                emit(Resource.error(data = null, message = exception.message ?: "Error"))
            }
        }


}