package io.betterapps.message.ui.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import io.betterapps.message.const.GlobalConstants
import io.betterapps.message.data.DetailRepository
import io.betterapps.message.domain.dto.Message
import io.betterapps.message.ui.BaseViewModel
import io.betterapps.messages.data.coroutines.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay

class DetailViewModel @ViewModelInject constructor(
    val repository: DetailRepository
) : BaseViewModel() {

    fun requestMessages(groupId: String): LiveData<Resource<List<Message>>> =
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
//            delay(2000)
            try {
                emit(Resource.success(repository.getMessages(groupId)))
            } catch (exception: Exception) {
                emit(Resource.error(data = null, message = exception.message ?: "Error"))
            }
        }


    fun replyMessage(msgFromUser: String): LiveData<Resource<String>> = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        delay(GlobalConstants.DELAY_REPLY)
        try {
            emit(Resource.success(msgFromUser.reversed()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error"))
        }
    }
}
