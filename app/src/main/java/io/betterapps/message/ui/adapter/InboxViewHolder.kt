package io.betterapps.message.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.betterapps.message.R
import io.betterapps.message.domain.dto.Inbox
import io.betterapps.message.utils.TimeUtils
import kotlinx.android.extensions.LayoutContainer

class InboxViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    companion object {
        fun from(parent: ViewGroup): InboxViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.viewholder_inbox, parent, false)
            return InboxViewHolder(view)
        }
    }

    // cache the viewIDs for once
    // avoid kotlinx synthetic views
    val topicTextView: TextView = itemView.findViewById(R.id.inbox_topic_textview)
    val msgTextView: TextView = itemView.findViewById(R.id.inbox_message_textview)
    val dateTextView: TextView = itemView.findViewById(R.id.inbox_date_textview)


    override val containerView: View?
        get() = itemView

    fun bindData(item: Inbox, listener: (Inbox) -> Unit) {
        topicTextView.text = item.topic
        msgTextView.text = item.lastMessage
        dateTextView.text = TimeUtils.formatLocalTime(item.modifiedAt)

        itemView.setOnClickListener {
            listener(item)
        }
    }

}

