package io.betterapps.message.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.betterapps.message.R
import io.betterapps.message.domain.dto.Message
import io.betterapps.message.utils.TimeUtils
import kotlinx.android.extensions.LayoutContainer

class DetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    companion object {
        fun from(parent: ViewGroup): DetailViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.viewholder_detail, parent, false)
            return DetailViewHolder(view)
        }
    }

    val senderTextView: TextView = itemView.findViewById(R.id.detail_sender_name_textview)
    val msgTextView: TextView = itemView.findViewById(R.id.detail_message_textview)
    val dateTextView: TextView = itemView.findViewById(R.id.detail_date_textview)


    override val containerView: View?
        get() = itemView

    fun bindData(item: Message) {
        senderTextView.text = item.sender
        msgTextView.text = item.message
        dateTextView.text = TimeUtils.formatLocalTime(item.modifiedAt)
    }
}