package io.betterapps.message.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import io.betterapps.message.R
import io.betterapps.message.domain.dto.Message
import io.betterapps.message.ui.BaseActivity
import io.betterapps.message.ui.adapter.DetailAdapter
import io.betterapps.messages.data.coroutines.Resource
import io.betterapps.messages.data.coroutines.Status
import kotlinx.android.synthetic.main.activity_detail.*
import timber.log.Timber

@AndroidEntryPoint
class DetailActivity : BaseActivity() {

    companion object {
        private val BUNDLE_MSG_ID = "MSG_ID_EXTRA"
        private val BUNDLE_TOPIC = "TOPIC_EXTRA"
        private val BUNDLE_MEMBERS = "MEMBERS_EXTRA"

        fun createIntent(
            context: Context,
            groupId: String,
            topic: String,
            members: List<String>
        ): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(BUNDLE_MSG_ID, groupId)
            intent.putExtra(BUNDLE_TOPIC, topic)
            intent.putStringArrayListExtra(BUNDLE_MEMBERS, ArrayList(members))
            return intent
        }
    }

    private lateinit var viewModel: DetailViewModel
    private lateinit var title: String
    private lateinit var groupId: String
    private lateinit var members: java.util.ArrayList<String>
    private lateinit var detailAdapter: DetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        viewModel = ViewModelProvider(this).get(DetailViewModel::class.java)

        val extras = intent.extras
        extras?.also {
            title = it.getString(BUNDLE_TOPIC, getString(R.string.detail))
            groupId = it.getString(BUNDLE_MSG_ID)!! // no way the Id is going to be Null
            members = it.getStringArrayList(BUNDLE_MEMBERS)!!
        }

        setTitle(title)
        Timber.d("groupId = ${groupId} , title = ${title}")
        viewModel.requestMessages(groupId)
            .observe(
                this,
                androidx.lifecycle.Observer {
                    it?.let { resource -> displayMessages(resource) }
                }
            )

        detail_send_button.setOnClickListener {
            val msg = detail_message_edittext.text.toString()
            addNewMessageToChat(msg, getString(R.string.me))

            // clear message and hide keyboard
            detail_message_edittext.setText("")
            detail_message_edittext.hideKeyboard()

            viewModel.replyMessage(msg)
                .observe(
                    this,
                    androidx.lifecycle.Observer {
                        it?.let { reply -> displayNewMessages(reply) }
                    }
                )
        }
    }


    private fun displayMessages(resource: Resource<List<Message>>): Unit {
        when (resource.status) {
            Status.LOADING -> {
                Timber.d("Loading")
                showProgressBar(true)
            }
            Status.SUCCESS -> {
                showProgressBar(false)
                resource.data?.let {
                    Timber.d("inboxes = ${it}")
                    setupRecyclerView(it)
                }
            }
            Status.ERROR -> {
                showProgressBar(false)
                Timber.d("Error ${resource.status} , ${resource?.message}")
            }
        }
    }


    private fun displayNewMessages(resource: Resource<String>): Unit {
        when (resource.status) {
            Status.LOADING -> {
                showProgressBar(true)
                Timber.d("Loading")
            }
            Status.SUCCESS -> {
                showProgressBar(false)
                resource.data?.let { addGroupReply(it) }
            }
            Status.ERROR -> {
                showProgressBar(false)
                Timber.d("Error ${resource.status} , ${resource?.message}")
            }
        }
    }

    private fun setupRecyclerView(response: List<Message>) {
        // add divider

        detailAdapter = DetailAdapter()
        detailAdapter.addAll(response)
        detail_recyclerview.apply {
            adapter = detailAdapter
        }
    }

    private fun addNewMessageToChat(msg: String, sender: String): Unit {
        detailAdapter.add(msg, sender)
        detailAdapter.notifyDataSetChanged()
    }

    private fun addGroupReply(msg: String): Unit {
        val memberName = members[rand(0, members.size - 1)]
        addNewMessageToChat(msg, memberName)
    }

    private fun rand(start: Int, end: Int): Int {
        require(start <= end) { "Illegal Argument" }
        return (Math.random() * (end - start + 1)).toInt() + start
    }


    private fun showProgressBar(enable: Boolean): Unit {
        if (enable) {
            detail_progress_progressbar.visibility = View.VISIBLE
        } else {
            detail_progress_progressbar.visibility = View.INVISIBLE
        }
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

}