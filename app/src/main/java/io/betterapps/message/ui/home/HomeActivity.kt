package io.betterapps.message.ui.home

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import io.betterapps.message.R
import io.betterapps.message.domain.dto.Inbox
import io.betterapps.message.ui.BaseActivity
import io.betterapps.message.ui.adapter.InboxAdapter
import io.betterapps.message.ui.detail.DetailActivity
import io.betterapps.messages.data.coroutines.Resource
import io.betterapps.messages.data.coroutines.Status
import kotlinx.android.synthetic.main.activity_home.*
import timber.log.Timber

@AndroidEntryPoint
class HomeActivity : BaseActivity() {

    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        setContentView(R.layout.activity_home)

        // fetch home inbox
        viewModel.requestHomeMessages()
            .observe(
                this,
                androidx.lifecycle.Observer {
                    it?.let { mes ->
                        displayMessages(mes)
                    }
                }
            )
        // fetch home inbox

    }


    private fun showErrorSnackBar() {
        Snackbar.make(coordinatorLayout, R.string.error_message, Snackbar.LENGTH_SHORT).show()
    }


    private fun displayMessages(resource: Resource<List<Inbox>>): Unit {
        when (resource.status) {
            Status.LOADING -> {
                showProgressBar(true)
                Timber.d("Loading")
            }
            Status.SUCCESS -> {
                showProgressBar(false)
                resource.data?.let {
                    Timber.d("inboxes = ${it}")
                    setupUI(it)
                }
            }
            Status.ERROR -> {
                showProgressBar(false)
                Timber.d("Error ${resource.status} , ${resource?.message}")
            }
        }
    }

    private fun setupUI(response: List<Inbox>) {
        // add divider
        val dividerItemDecoration = DividerItemDecoration(
            inbox_recyclerview.context,
            LinearLayout.HORIZONTAL
        )
        inbox_recyclerview.addItemDecoration(dividerItemDecoration)

        val weatherAdapter = InboxAdapter(response, { inbox: Inbox -> navigateToMovie(inbox) })
        inbox_recyclerview.apply {
            adapter = weatherAdapter
        }
    }


    private fun navigateToMovie(item: Inbox) {
        val intent = DetailActivity.createIntent(this, item.id, item.topic, item.members)
        startActivity(intent)
    }

    private fun showProgressBar(enable: Boolean): Unit {
        home_progress_progressbar.visibility = if (enable) View.VISIBLE else View.INVISIBLE
    }
}