package io.betterapps.message.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.betterapps.message.domain.dto.Inbox
import timber.log.Timber

/**
 * This adapter data won't be updated.
 * Hence the data are passed by constructor
 */
class InboxAdapter(val data: List<Inbox>,  val listener: (Inbox) -> Unit) : RecyclerView.Adapter<InboxViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InboxViewHolder {
        return InboxViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: InboxViewHolder, position: Int) {
        holder.apply {
            bindData(data.get(position), listener)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }
}
