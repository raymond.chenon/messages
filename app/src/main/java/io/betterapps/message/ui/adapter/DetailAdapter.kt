package io.betterapps.message.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.betterapps.message.domain.dto.Message
import timber.log.Timber
import java.time.Instant

class DetailAdapter() : RecyclerView.Adapter<DetailViewHolder>() {

    val data: MutableList<Message>

    init {
        data = mutableListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        return DetailViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.apply {
            bindData(data.get(position))
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun addAll(messages: List<Message>){
        data.addAll(messages)
    }

    fun add(msgContent: String, sender: String): Unit {
        val msgId = data[data.lastIndex].id.toLong() + 1
        val time = Instant.now().toEpochMilli()
        Timber.d("timestamp = ${time}")
        val message = Message(msgId.toString(), msgContent, time, sender)
        data.add(message)
    }
}
