package io.betterapps.message.const

object GlobalConstants {

    const val BASE_URL = "https://idtm-media.s3.amazonaws.com/programming-test/api/"

    const val DELAY_REPLY = 2000L // 2 seconds
}