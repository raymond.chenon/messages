package io.betterapps.message.domain.dto

import com.google.gson.annotations.SerializedName

// https://idtm-media.s3.amazonaws.com/programming-test/api/inbox.json
data class Inbox(
    @SerializedName("id") val id: String,
    @SerializedName("last_message") val lastMessage: String,
    @SerializedName("members") val members: List<String>,
    @SerializedName("topic") val topic: String,
    @SerializedName("modified_at") val modifiedAt: Long,
)