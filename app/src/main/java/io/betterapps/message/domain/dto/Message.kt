package io.betterapps.message.domain.dto

import com.google.gson.annotations.SerializedName

// https://idtm-media.s3.amazonaws.com/programming-test/api/9991.json
data class Message(
    @SerializedName("id") val id: String,
    @SerializedName("message") val message: String,
    @SerializedName("modified_at") val modifiedAt: Long,
    @SerializedName("sender") val sender: String
)