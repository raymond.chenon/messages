package io.betterapps.message.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {

    private val dayFormat = instantiateDateFormat()

    @JvmStatic
    fun formatLocalTime(dateTime: Long, timeOffset: Long = 0): String {
        val date = java.util.Date((dateTime + timeOffset))
        return dayFormat.format(date)
    }

    private fun instantiateDateFormat(format: String = "HH:mm"): SimpleDateFormat {
        val sdf = SimpleDateFormat(format)
        sdf.setTimeZone(TimeZone.getTimeZone("GMT")) // IMPORTANT: set time zone independent of the local machine
        return sdf
    }

}