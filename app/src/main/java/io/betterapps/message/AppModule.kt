package io.betterapps.message

import android.content.Context
import androidx.preference.PreferenceManager
import com.tfcporciuncula.flow.FlowSharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import io.betterapps.message.data.network.DetailService
import io.betterapps.message.data.network.InboxService
import io.betterapps.message.data.network.RetrofitFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun flowSharedPreferences(@ApplicationContext appContext: Context) =
        FlowSharedPreferences(PreferenceManager.getDefaultSharedPreferences(appContext))

    @Provides
    @Singleton
    fun inboxService(): InboxService = RetrofitFactory.homeService()

    @Provides
    @Singleton
    fun detailService(): DetailService = RetrofitFactory.detailService()

}